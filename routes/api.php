<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Tool API Routes
|--------------------------------------------------------------------------
|
| Here is where you may register API routes for your tool. These routes
| are loaded by the ServiceProvider of your tool. They are protected
| by your tool's "Authorize" middleware by default. Now, go build!
|
*/

Route::get('/authenticate', 'Marshmallow\Accounting\Accountable\Http\Controllers\AuthenticationController@authenticate');
Route::get('/disconnect', 'Marshmallow\Accounting\Accountable\Http\Controllers\AuthenticationController@disconnect');
Route::get('/validate', 'Marshmallow\Accounting\Accountable\Http\Controllers\AuthenticationController@validateConnection');
Route::get('/me', 'Marshmallow\Accounting\Accountable\Http\Controllers\AuthenticationController@me');
Route::get('/connected', 'Marshmallow\Accounting\Accountable\Http\Controllers\AuthenticationController@connected');
Route::post('/show', 'Marshmallow\Accounting\Accountable\Http\Controllers\AccountableController@show');