Nova.booting((Vue, router, store) => {
  Vue.component('index-accountable-field', require('./components/IndexField'))
  Vue.component('detail-accountable-field', require('./components/DetailField'))
  Vue.component('form-accountable-field', require('./components/FormField'))
})
