<?php

namespace Marshmallow\Accounting\Accountable;

use Exception;
use Marshmallow\Accounting\Accountable\AccountableField;
use Marshmallow\Accounting\Accountable\AccountableResourceTool;

class Accounting
{
	protected $service;

	public function __construct ($settings)
	{
		$this->service = new $settings['service'];
	}

	public function __get ($parameter)
	{
		switch ($parameter) {
			case 'service':
				return $this->service;
				break;
		}
		throw new Exception(
			"Cannot access protected property Marshmallow\Accounting\Accountable\Accounting::$parameter"
		);
	}
}
