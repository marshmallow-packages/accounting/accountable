<?php

namespace Marshmallow\Accounting\Accountable\Models;

use Illuminate\Database\Eloquent\Model;

class AccountableData extends Model
{
	protected $guarded = [];
}
