<?php

namespace Marshmallow\Accounting\Accountable\Models;

use Exception;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Picqer\Financials\Exact\Connection;
use Marshmallow\Accounting\Accountable\Contracts\AccountableInterface;

class AccountableConnect extends Model
{
    protected $primaryKey = 'service';

	public $timestamps = false;
    
    protected $guarded = [];

    public static function getToken (AccountableInterface $service)
    {
        return self::where('service', get_class($service))->first();
    }

    public static function storeToken (AccountableInterface $service)
    {
        if ($token = self::getToken($service)) {
            $token->delete();
        }

        return self::create([
            'service' => get_class($service),
            'access_token' => $service->getAccessToken(),
            'refresh_token' => $service->getRefreshToken(),
            'token_expires' => $service->getTokenExpires(),
        ]);
    }

    public function getKeyType()
    {
        return 'string';
    }
}
