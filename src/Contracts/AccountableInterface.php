<?php 

namespace Marshmallow\Accounting\Accountable\Contracts;

use Carbon\Carbon;
use Marshmallow\Ecommerce\Cart\Models\Inquiry;
use Marshmallow\Ecommerce\Cart\Models\Prospect;
use Marshmallow\Accounting\Accountable\Classes\AccountableModel;
use Marshmallow\Accounting\Accountable\Http\Resources\MeResource;
use Marshmallow\Accounting\Accountable\Http\Resources\AuthenticateResource;
use Marshmallow\Accounting\Accountable\Http\Resources\CreateInquiryResource;
use Marshmallow\Accounting\Accountable\Http\Resources\CreateProspectResource;

interface AccountableInterface
{
	public function me (): MeResource;
	public function getAccessToken (): string;
	public function getRefreshToken (): string;
	public function getAuthUrl (): AuthenticateResource;
	public function getTokenExpires (): Carbon;
	public function getProduct ($product_accountable_id): AccountableModel;
	public function createInquiry (Inquiry $inquiry): CreateInquiryResource;
	public function createProspect (Prospect $prospect): CreateProspectResource;
}