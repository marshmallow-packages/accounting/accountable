<?php

namespace Marshmallow\Accounting\Accountable;

use Laravel\Nova\Nova;
use Laravel\Nova\Events\ServingNova;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Marshmallow\Accounting\Accountable\Accounting;
use Marshmallow\Accounting\Accountable\Console\InstallCommand;
use Marshmallow\Accounting\Accountable\Http\Middleware\Authorize;

class ToolServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(Accounting::class, function () {
            return new Accounting([
                'service' => config('services.accounting.service'),
            ]);
        });
        
        $this->app->alias(Accounting::class, 'accounting');

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'accountable');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->mergeConfigFrom(
            __DIR__ . '/../config/accountable.php', 'accountable'
        );
        
        $this->publishes([
            __DIR__ . '/../database/migrations' => database_path('migrations'),
        ], 'migrations');

        $this->publishes([
            __DIR__ . '/../config/accountable.php' => config_path('accountable.php'),
        ], 'config');

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'accountable');

        $this->app->booted(function () {
            $this->routes();
        });

        Nova::serving(function (ServingNova $event) {
            Nova::script('accountable-card', __DIR__.'/../dist/js/card.js');
            Nova::style('accountable-card', __DIR__.'/../dist/css/card.css');
            Nova::script('accountable-field', __DIR__.'/../dist/js/field.js');
            Nova::style('accountable-field', __DIR__.'/../dist/css/field.css');
            Nova::script('accountable-resource-tool', __DIR__.'/../dist/js/resource-tool.js');
            Nova::style('accountable-resource-tool', __DIR__.'/../dist/css/resource-tool.css');
        });

        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
            ]);
        }
    }

    /**
     * Register the tool's routes.
     *
     * @return void
     */
    protected function routes()
    {
        if ($this->app->routesAreCached()) {
            return;
        }

        Route::middleware(['nova', Authorize::class])
                ->prefix('nova-vendor/accountable')
                ->group(__DIR__.'/../routes/api.php');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
