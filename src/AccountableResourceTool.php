<?php

namespace Marshmallow\Accounting\Accountable;

use Laravel\Nova\ResourceTool;

class AccountableResourceTool extends ResourceTool
{
    /**
     * Get the displayable name of the resource tool.
     *
     * @return string
     */
    public function name()
    {
        return 'Accountable Resource Tool';
    }

    /**
     * Get the component name for the resource tool.
     *
     * @return string
     */
    public function component()
    {
        return 'accountable-resource-tool';
    }
}
