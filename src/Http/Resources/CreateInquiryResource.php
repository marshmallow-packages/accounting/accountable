<?php

namespace Marshmallow\Accounting\Accountable\Http\Resources;

use Marshmallow\Ecommerce\Cart\Models\Inquiry;
use Illuminate\Http\Resources\Json\JsonResource;

class CreateInquiryResource extends JsonResource
{
	public function __construct($accounting_id, $accountable_resource, Inquiry $inquiry)
    {
        $this->resource = $accountable_resource;

        /**
         * Set up the connection so we know this inquiry
         * is already available in the accounting software.
         */
        $inquiry->accountable()->create([
            'accounting_id' => $accounting_id,
            'accounting_last_sync' => now(),
        ]);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        	'success' => true,
            'inquiry' => $this->resource,
        ];
    }
}
