<?php

namespace Marshmallow\Accounting\Accountable\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
        	'success' => true,
            'user' => $this->resource,
            'visible_user_fields' => config('accountable.visible_user_fields'),
        ];
    }
}
