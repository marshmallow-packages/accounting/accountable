<?php

namespace Marshmallow\Accounting\Accountable\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AuthenticateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'auth_url' => $this->resource,
        ];
    }
}
