<?php

namespace Marshmallow\Accounting\Accountable\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountableController extends Controller
{
    public function show (Request $request)
    {
    	$model_name = $request->model;
    	$model = $model_name::find($request->resourceId);

    	if ($model->accountable) {
    		return response()->json([
	    		'data' => $model->getDataFromAccountable()->getData(),
	    		'fields' => $model->getAccountableModelFields(),
	    	]);
    	}

    	return response()->json([
    		'error' => 'There is no accountable data available to show here'
    	]);
    	
    }
}
