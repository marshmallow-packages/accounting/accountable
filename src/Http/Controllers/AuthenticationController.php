<?php

namespace Marshmallow\Accounting\Accountable\Http\Controllers;

use Exception;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Marshmallow\Accounting\Accountable\Http\Resources\MeResource;
use Marshmallow\Accounting\Accountable\Models\AccountableConnect;

class AuthenticationController extends Controller
{
    public function connected (Request $request)
    {
        try {
            $me = $this->me($request);
            if ($me instanceof MeResource) {
                return response()->json([
                    'connected' => true
                ]);
            }
        } catch (Exception $e) {
            //
        }
        return response()->json([
            'connected' => false
        ]);
    }

    public function authenticate (Request $request)
    {
        return response()->json(
            app('accounting')->service->getAuthUrl()
        );
    }

    public function disconnect (Request $request)
    {
        $connection_auth = AccountableConnect::getToken(app('accounting')->service);
        if (!$connection_auth) {
            throw new Exception('Er is geen verbinding om te verbreken');
        }

        $connection_auth->delete();

        return response()->json([
            'success' => true,
        ]);
    }

    public function validateConnection (Request $request)
    {
        try {
            return app('accounting')->service->authCallback($request->code);
        } catch (Exception $e) {
            throw new Exception('Could not connect to accountable: ' . $e->getMessage());
        }
    }

    public function me(Request $request)
    {
        try {
            return app('accounting')->service->me();
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'exception' => $e->getMessage()
            ]);
        }
    }
}
