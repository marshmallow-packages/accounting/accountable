<?php

namespace Marshmallow\Accounting\Accountable\Helpers;
use Illuminate\Database\Schema\Blueprint;

class ConfigHelper
{
	public static function mapping ($mapping)
	{
		return config('accountable.mapping.' . $mapping);
	}
}