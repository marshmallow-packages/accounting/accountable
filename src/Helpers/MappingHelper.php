<?php

namespace Marshmallow\Accounting\Accountable\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Schema\Blueprint;
use Marshmallow\Accounting\Accountable\Helpers\ConfigHelper;

class MappingHelper
{
	public static function map ($accountable_model, Model $our_model, $mapping_name)
	{
		$mapping = ConfigHelper::mapping($mapping_name);

		foreach ($mapping as $accountable_field => $model_field) {
            if (is_array($model_field)) {
                $relationship = $model_field[0];
                $model_field = $model_field[1];
                $accountable_model->{$accountable_field} = $our_model->{$relationship}->$model_field;
            } else {
                $accountable_model->{$accountable_field} = $our_model->{$model_field};
            }
        }

        return $accountable_model;
	}
}