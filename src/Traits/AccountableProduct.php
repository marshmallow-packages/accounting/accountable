<?php

namespace Marshmallow\Accounting\Accountable\Traits;

use Picqer\Financials\Exact\Item;
use Marshmallow\Accounting\Accountable\Models\AccountableConnect;

trait AccountableProduct
{
    public function getDataFromAccountable ()
    {
        return app('accounting')->service->getProduct($this->accountable->accounting_id);
    }

    protected function viewOnDetail ()
    {
    	return ['Code', 'ID', 'Description'];
    }
}