<?php

namespace Marshmallow\Accounting\Accountable\Traits;

use Picqer\Financials\Exact\Account;
use Marshmallow\Accounting\Accountable\Models\AccountableConnect;
use Marshmallow\Accounting\Accountable\Jobs\CreateAccountInAccountable;

trait AccountableCustomer
{
    public function syncToAccounting ()
    {
        if ($this->accountable) {
            // Update?
        } else {
            return app('accounting')->service->createProspect($this);
        }
    }

    public function getDataFromAccountable ()
    {
        $account = $this->getAccountableModel();
        return $account->find($this->accountable->accounting_id);
    }

    protected function viewOnIndex ()
    {
        return ['ID', 'Name'];
    }
}