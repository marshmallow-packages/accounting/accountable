<?php

namespace Marshmallow\Accounting\Accountable\Traits;

use Picqer\Financials\Exact\Account;
use Marshmallow\Accounting\Accountable\Models\AccountableData;
use Marshmallow\Accounting\Accountable\Models\AccountableConnect;

trait Accountable
{
	public function accountable ()
    {
    	return $this->morphOne(AccountableData::class, 'accountable');
    }

    public function getAccountableModelFields ()
    {
        return $this->viewOnDetail();
    }
}