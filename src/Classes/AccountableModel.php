<?php

namespace Marshmallow\Accounting\Accountable\Classes;

class AccountableModel
{
	protected $id;
	protected $data;

	public function __construct ($accountable_id, $accountable_data)
	{
		$this->id = $accountable_id;
		$this->data = $accountable_data;
	}

	public function getId ()
	{
		return $this->id;
	}

	public function getData ()
	{
		return $this->data;
	}
}