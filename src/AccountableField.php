<?php

namespace Marshmallow\Accounting\Accountable;

use Laravel\Nova\Fields\Field;

class AccountableField extends Field
{
    /**
     * The field's component.
     *
     * @var string
     */
    public $component = 'accountable-field';
}
