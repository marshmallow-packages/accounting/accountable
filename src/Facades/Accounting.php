<?php 

namespace Marshmallow\Accounting\Accountable\Facades;

class Accounting extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return \Marshmallow\Accounting\Accountable\Accounting::class;
    }
}